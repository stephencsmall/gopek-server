# gopek-server

A stupid web service, written in Go.

## Usage

This is a web service that will return a JSON containing a hilarious randomly-generated Popek name.

`docker run --rm -it -p 8888:8888 stephencsmall/gopek-server`

Get a response by doing a `GET` against `/popek`:

`curl -X GET localhost:8888/popek`