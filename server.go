package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type Popek struct {
	Name string `json:"name"`
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/popek", popek).Methods("GET")
	log.Fatal(http.ListenAndServe(":8888", router))

}

func popek(writer http.ResponseWriter, reader *http.Request) {

	log.Println("Request from : " + reader.UserAgent() + ", " + reader.Host)
	outgoingJSON, err := json.Marshal(&Popek{Name: randPek()})

	if err != nil {
		log.Println(err.Error())
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}

	writer.Header().Set("Content-Type", "application/json")
	fmt.Fprint(writer, string(outgoingJSON))
}

func randPek() string {
	PATwords := []string{"Pat", "Pot", "Poot", "Pro", "Pom", "Pop", "Porta", "Pop", "Pho", "Pam", "Patty", "Patak", "Pad"}
	RICKwords := []string{"rick", "rock", "ronk", "rook", "rank", "root", "rock", "pick", "stick", "rickly"}
	POwords := []string{"Po", "Pro", "Lo", "Sno", "Poo", "No", "Pee", "Jingle", "Jangle", "Pie", "Toe"}
	PEKwords := []string{"pek", "pok", "pook", "prok", "pram", "deck", "fleck", "check", "chop", "tech", "gasm"}

	rand.Seed(time.Now().UnixNano())

	FirstnamePrefix := PATwords[rand.Intn(len(PATwords)-1)]
	FirstnameSuffix := RICKwords[rand.Intn(len(RICKwords)-1)]
	LastnamePrefix := POwords[rand.Intn(len(POwords)-1)]
	LastnameSuffix := PEKwords[rand.Intn(len(PEKwords)-1)]

	name := FirstnamePrefix + FirstnameSuffix + " " + LastnamePrefix + LastnameSuffix
	return name
}
